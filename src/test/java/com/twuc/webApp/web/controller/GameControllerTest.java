package com.twuc.webApp.web.controller;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @BeforeEach
    void setUp() {

    }
    //测试要分开测试，因为gameID是自增的，测试之间会相互影响
    //TODO: 测试覆盖率不足，测试不仅要包含happy path，还要有alternative path和sad path
    @Test
    void test_create_game() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/games/1"));

        mockMvc.perform(post("/api/games"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/games/2"));
    }

    @Test
    void test_play_game() throws Exception {

        mockMvc.perform(post("/api/games"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/games/1"));
        mockMvc.perform(patch("/api/games/1").contentType(MediaType.APPLICATION_JSON_VALUE).content("{ \"answer\": \"1234\"}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.correct").value(Matchers.any(Boolean.class)))
                .andExpect(jsonPath("$.hint").exists());

    }

    @Test
    void test_get_game_status() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/games/1"));
        MvcResult result = mockMvc.perform(get("/api/games/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value("1")).andReturn();

        mockMvc.perform(get("/api/games/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(content().string(result.getResponse().getContentAsString()));

    }
}
