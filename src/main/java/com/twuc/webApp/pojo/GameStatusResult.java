package com.twuc.webApp.pojo;

public class GameStatusResult {

    private Integer id;
    private String answer;

    public GameStatusResult() {
    }

    public GameStatusResult(Integer id, String answer) {
        this.id = id;
        this.answer = answer;
    }

    public Integer getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }
}
