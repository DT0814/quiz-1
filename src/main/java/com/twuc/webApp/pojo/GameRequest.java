package com.twuc.webApp.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class GameRequest {
    @Size(min = 4, max = 4)
    @NotNull
    private String answer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
