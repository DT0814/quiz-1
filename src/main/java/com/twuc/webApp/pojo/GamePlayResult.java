package com.twuc.webApp.pojo;

public class GamePlayResult {
    private String hint;
    private Boolean correct;

    public GamePlayResult(String hint, Boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

    public GamePlayResult() {
    }

    public String getHint() {
        return hint;
    }

    public Boolean getCorrect() {
        return correct;
    }
}
