package com.twuc.webApp.service;

import com.twuc.webApp.pojo.GamePlayResult;
import com.twuc.webApp.pojo.GameStatusResult;
import org.omg.CORBA.INTERNAL;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Service
//TODO: 注意下变量命名、变量抽取和方法抽取
public class GameService {
    private static final String GAME_KEY_ID = "gameID";

    private static final String GAMES_KEY = "games";

    private static final Integer answerLen = 4;

    public Integer createGame(ServletContext context) {
        Integer res = (Integer) context.getAttribute(GAME_KEY_ID);

        if (null == res) {
            res = 1;
            context.setAttribute(GAMES_KEY, new HashMap<Integer, INTERNAL>());
        }
        context.setAttribute(GAME_KEY_ID, res + 1);

        Map<Integer, Integer> games = (Map<Integer, Integer>) context.getAttribute(GAMES_KEY);

        games.put(res, createAnswer(answerLen));
        context.setAttribute(GAMES_KEY, games);
        return res;
    }

    private Integer createAnswer(int length) {

        StringBuilder answer = new StringBuilder();
        Random random = new Random();
        while (answer.length() < length) {
            int i = random.nextInt(9) + 1;
            if (answer.indexOf("" + i) == -1) {
                answer.append(i);
            }
        }
        return Integer.parseInt(answer.toString());
    }

    public GamePlayResult playGame(ServletContext context, Integer gameId, String answer) {

        Map<Integer, Integer> games = (Map<Integer, Integer>) context.getAttribute(GAMES_KEY);
        if (null != games && games.containsKey(gameId)) {
            Integer right = games.get(gameId);
            Integer aValue = 0;
            Integer bValue = 0;
            boolean correct = false;
            char[] chars = right.toString().toCharArray();
            for (int i = 0; i < chars.length; i++) {
                if (chars[i] == answer.charAt(i)) {
                    aValue++;
                }
                if (answer.indexOf(chars[i]) != -1) {
                    bValue++;
                }
            }
            if (aValue.equals(answerLen)) {
                correct = true;
            }
            return new GamePlayResult(aValue + "A" + (bValue - aValue) + "B", correct);
        }

        return new GamePlayResult("$$$$", false);
    }

    public GameStatusResult getGameStatus(Integer gameId, ServletContext context) {

        Map<Integer, Integer> games = (Map<Integer, Integer>) context.getAttribute(GAMES_KEY);
        if (games.containsKey(gameId)) {
            Integer integer = games.get(gameId);
            return new GameStatusResult(gameId, integer + "");
        }

        return new GameStatusResult(gameId, "gameId Not found");
    }
}

