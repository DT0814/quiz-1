package com.twuc.webApp.web.controller;

import com.twuc.webApp.pojo.GameRequest;
import com.twuc.webApp.pojo.GamePlayResult;
import com.twuc.webApp.pojo.GameStatusResult;
import com.twuc.webApp.service.GameService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @author tao.dong
 */
@RestController
@RequestMapping("/api")
public class GameController {
    private GameService service;

    public GameController(GameService service) {
        this.service = service;
    }


    @PostMapping("/games")
    public ResponseEntity createGame(HttpSession session) {
        ServletContext servletContext = session.getServletContext();
        Integer gameId = service.createGame(servletContext);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("Location", "/api/games/" + gameId)
                .build();
    }

    @PatchMapping("/games/{gameId}")
    public ResponseEntity playGame(@PathVariable Integer gameId, @Valid @RequestBody GameRequest gameRequest, HttpSession session) {
        ServletContext servletContext = session.getServletContext();

        GamePlayResult gameResult = service.playGame(servletContext, gameId, gameRequest.getAnswer());

        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(gameResult);
    }


    @GetMapping("/games/{gameId}")
    public ResponseEntity getGameStatus(@PathVariable Integer gameId, HttpSession session) {
        ServletContext servletContext = session.getServletContext();

        GameStatusResult gameResult = service.getGameStatus(gameId, servletContext);

        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(gameResult);
    }
}
